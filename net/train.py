import urllib2 # for downloading the dataset from the web.
import numpy as np
from matplotlib import pyplot
from StringIO import StringIO
from caffe2.python import core, utils, workspace,model_helper,brew,net_drawer,cnn,visualize
from caffe2.proto import caffe2_pb2
import pickle
import lmdb
import caffe2.python.predictor.predictor_exporter as pe
from IPython import display
import cv2
import os,sys
from xml.dom import minidom
import xml.etree.ElementTree as ET

def CreateFile(labels):

	# create the file structure
	data = ET.Element('OutputLabels')  
	items = ET.SubElement(data, 'Labels') 
 	
	for i,lbl in enumerate(labels):
		
		item2 = ET.SubElement(items, 'item')
		item2.set('id',str(i))
		item2.text = lbl.split(".")[0]
		print item2.text
  
	# create a new XML file with the results
	mydata = ET.tostring(data)  
	myfile = open("./OutputLabels.xml", "w")  
	myfile.write(mydata) 


# Function to construct a MLP neural network
# The input 'model' is a model helper and 'data' is the input data blob's name
def AddMLPModel(model, data,output_labels):
 
    size = 128
    sizes = [size, size*2, size*2, size*2, size*2, size*2, size*2, size*2, output_labels]

    layer = data
    for i in range(len(sizes) - 1):
        layer = brew.fc(model, layer, 'dense_{}'.format(i), dim_in=sizes[i], dim_out=sizes[i + 1])
	#layer = brew.prelu(model, layer, 'prelu_{}'.format(i))
        layer = brew.sum(model, layer, 'relu_{}'.format(i))
        #layer = brew.tanh(model, layer, 'relu2_{}'.format(i))
    
    softmax = brew.softmax(model, layer, 'softmax')
    return softmax



def AddInput(model, batch_size, db, db_type):
    # load the data

    data, label = model.TensorProtosDBInput(
        [], ["data", "label"], batch_size=batch_size,db=db, db_type=db_type)

    # don't need the gradient for the backward pass
    data = model.StopGradient(data, data)

    return data, label


def AddAccuracy(model, softmax, label):
    """Adds an accuracy op to the model"""
    accuracy = brew.accuracy(model, [softmax, label], "accuracy")
    return accuracy

def AddTrainingOperators(model, softmax, label):
    """Adds training operators to the model."""
    xent = model.LabelCrossEntropy([softmax, label], 'xent')

    # compute the expected loss
    loss = model.AveragedLoss(xent, "loss")
 
   # track the accuracy of the model
    AddAccuracy(model, softmax, label)

    # use the average loss we just computed to add gradient operators to the model
    model.AddGradientOperators([loss])

    # do a simple stochastic gradient descent
    ITER = brew.iter(model, "iter")

    # set the learning rate schedule
    LR = model.LearningRate(
        ITER, "LR", base_lr=-0.01, policy="step", stepsize=1, gamma=0.999 )
   
    # ONE is a constant value that is used in the gradient update. We only need
    # to create it once, so it is explicitly placed in param_init_net.
   
    ONE = model.param_init_net.ConstantFill([], "ONE", shape=[1], value=1.0)
   
    # Now, for each parameter, we do the gradient updates.
    for param in model.params:
        # Note how we get the gradient of each parameter - ModelHelper keeps
        # track of that.
        param_grad = model.param_to_grad[param]
        # The update is a simple weighted sum: param = param + param_grad * LR
        model.WeightedSum([param, ONE, param_grad, LR], param)

def AddBookkeepingOperators(model):    
    # Print basically prints out the content of the blob. to_file=1 routes the
    # printed output to a file. The file is going to be stored under
    #     root_folder/[blob name]
    model.Print('accuracy', [], to_file=1)
    model.Print('loss', [], to_file=1)
    # Summarizes the parameters. Different from Print, Summarize gives some
    # statistics of the parameter, such as mean, std, min and max.
    for param in model.params:
        model.Summarize(param, [], to_file=1)
        model.Summarize(model.param_to_grad[param], [], to_file=1)


def CreateDataBase(features, labels, items, db_name):
	
	LMDB_MAP_SIZE = 104857600
	env = lmdb.open(db_name, map_size=LMDB_MAP_SIZE)

	with env.begin(write=True) as txn:

		# Move all data to the database
		for i in range(items):
			feature_and_label = caffe2_pb2.TensorProtos()

		        feature_and_label.protos.extend([
		            utils.NumpyArrayToCaffe2Tensor(features[i].astype(np.float32)),
		            utils.NumpyArrayToCaffe2Tensor(labels[i])])
			#print(feature_and_label)			
            		txn.put('{}'.format(i).encode('ascii'),feature_and_label.SerializeToString())


def save_net(INIT_NET, PREDICT_NET, model) :

    with open(PREDICT_NET, 'wb') as f:
        f.write(model.net._net.SerializeToString())
    init_net = caffe2_pb2.NetDef()

    for param in model.params:
        blob = workspace.FetchBlob(param)
        shape = blob.shape
        op = core.CreateOperator("GivenTensorFill", [], [param],arg=[ utils.MakeArgument("shape", shape),utils.MakeArgument("values", blob)])
        init_net.op.extend([op])
    init_net.op.extend([core.CreateOperator("ConstantFill", [], ["data"], shape=(1,28,28))])

    with open(INIT_NET, 'wb') as f:
	f.write(init_net.SerializeToString())




# If you would like to see some really detailed initializations,
# you can change --caffe2_log_level=0 to --caffe2_log_level=-1
core.GlobalInit(['caffe2', '--caffe2_log_level=0'])
print("Necessities imported!")

dataset_list = []

for filename in os.listdir('../Embeddings'):
	dataset_list.append(filename)

CreateFile(dataset_list)

print(dataset_list)

outputs = len(dataset_list)
base_features = []
base_labels = []

print("Outputs:", outputs)

for j in range(outputs):
	dir_path = '../Embeddings/'+dataset_list[j]
	dataset = pickle.loads(open(dir_path, "rb").read())
	emb = dataset["embeddings"]
	lbl = dataset["labels"]

	for i in range(len(lbl)):
		base_features.append(emb[i])
		base_labels.append(j)

total = len(base_labels)

features = np.zeros((total, 128), dtype=np.float32)
labels = np.zeros(total, dtype=np.int)

random_index = np.random.permutation(total)

for i in range(total):
	idx = random_index[i]
	features[i] = base_features[idx]
	labels[i] = base_labels[idx]

train_features = features[:int(total*0.85)]
train_labels = labels[:int(total*0.85)]
test_features = features[int(total*0.15):]
test_labels = labels[int(total*0.15):]

CreateDataBase(train_features, train_labels, len(train_labels), "./train_lmdb")
CreateDataBase(test_features, test_labels, len(test_labels), "./test_lmdb")



batch_size = 16
input_size = 128

arg_scope = {"order": "NCHW"}

model = model_helper.ModelHelper(name="mlp_train", arg_scope=arg_scope,init_params=True)
data, label = AddInput(model, batch_size=batch_size,db="./train_lmdb",db_type='lmdb')
softmax = AddMLPModel(model, data,outputs)
AddTrainingOperators(model, softmax, label)
AddBookkeepingOperators(model)


workspace.RunNetOnce(model.param_init_net)
workspace.CreateNet(model.net)

total_iters = 3000
accuracy = np.zeros(total_iters)
loss = np.zeros(total_iters)

print("We will manually run the network for {} iterations.".format(total_iters))
for i in range(total_iters):
    print(i)
    workspace.RunNet(model.net.Proto().name)
    accuracy[i] = workspace.FetchBlob('accuracy')
    loss[i] = workspace.FetchBlob('loss')


# After the execution is done, let's plot the values.
"""
pyplot.plot(loss, 'b')
pyplot.plot(accuracy, 'r')
pyplot.legend(('Loss', 'Accuracy'), loc='upper right')
pyplot.show()
"""


test_model = model_helper.ModelHelper(name="mlp_test", arg_scope=arg_scope, init_params=False)
data, label = AddInput(test_model, batch_size=1,db="./test_lmdb",db_type='lmdb')
softmax = AddMLPModel(test_model, data,outputs)
AddAccuracy(test_model, softmax, label)

#test_model = model

# run a test pass on the test net
workspace.RunNetOnce(test_model.param_init_net)
workspace.CreateNet(test_model.net, overwrite=True)
test_accuracy = np.zeros(len(test_labels))

for i in range(len(test_labels)):
	workspace.RunNet(test_model.net.Proto().name)
	test_accuracy[i] = workspace.FetchBlob('accuracy')
	print(workspace.FetchBlob("label"))


# After the execution is done, let's plot the values.
"""
pyplot.plot(test_accuracy, 'r')
pyplot.title('Acuracy over test batches.')
pyplot.show()
"""
print('test_accuracy: %f' % test_accuracy.mean())

# Deployment model. We simply need the main LeNetModel part.
deploy_model = model_helper.ModelHelper(name="face_cnn_deploy", arg_scope=arg_scope, init_params=False)
AddMLPModel(deploy_model, "data",outputs)

save_net('./init_net.pb', './predict_net.pb', deploy_model)

sys.exit(0)