# Treinamento de CNN para reconhecimento facial

Este artigo tem como objetivo exemplificar e demosntrar o processo de treinamento de uma rede convolucional utilizando o framework Caffe2, para aplicações de reconhecimento facial. 

# **Ambiente de Treinamento (PC - Linux)**

Para realizar o treinamento da CNN é necessário instalar o Anaconda na máquina de desenvolvimento.

#### - Passo 1: Instalação do anaconda2 para o python 2.7

Realizar download: https://www.anaconda.com/download/#linux
Para outras arquiteturas, verifique as opções no link indicado.

```sh
cd /home/${USER}
wget https://repo.continuum.io/archive/Anaconda2-2018.12-Linux-x86_64.sh
```

Comando para instalação: https://conda.io/docs/user-guide/install/linux.html

```sh
bash Anaconda2-2018.12-Linux-x86_64.sh
```

Confirme a licença e o diretório de instalação (/home/${USER}/anaconda2). Em seguida, teste o utilitário.

```sh
source ~/.bashrc
conda info
```

#### - Passo 2: Instalação do Opencv na máquina de desenvolvimento

```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
sudo apt-get install python-opencv
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module
```


#### - Passo 3: Instalação do Caffe2 na máquina de desenvolvimento

Realizar o download da biblioteca lmdb:

```sh
sudo apt-get update 
sudo apt-get install liblmdb-dev
```

Realizar o download do pytorch.

```sh
$ cd /home/${USER}
$ git clone --recursive https://github.com/pytorch/pytorch.git && cd pytorch
$ git submodule update --init --recursive
```

No diretório pytorch, execute o script de compilação do caffe2: 

```sh
./scripts/build_local.sh -DUSE_LMDB=ON
cd build
sudo ninja install
```

Cabe ressaltar que a variável de ambiente PYTHONPATH deve ser atualizada contendo o diretório do caffe2.

```sh
export PYTHONPATH=/home/$USER/pytorch/build:$PYTHONPATH
```

#### - Passo 4: Configuração do Python 2.7

Necessário instalar os pacotes do Python 2.7:

```sh
pip install imutils
pip install matplotlib
pip install numpy==1.15.0
pip install face_recognition
pip install google
pip install lmdb
pip install IPython
```

# Aplicação

Download do repositório:
```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2
cd mlp-for-facial-recognition-using-caffe2
```


## Aplicação para criar uma base de dados

Pode ser realizado de duas maneiras:
1. Diretamente de um dispositivo.
2. De um diretório com imagens.

Idependente das abordagens o resultado será um arquivo com os pontos faciais (embeddings) armazenado no diretório Embeddings. Posteriormente, esse diretório é usado para o treinamento da rede neural.

### Primeira abordagem

O seguinte comando iniciará a aplicação para capturar 30 imagens usando o dispositivo representado por /dev/video1. O arquivo com os pontos faciais serão gravados no diretório Embeddings com o nome foobar.pickle.
```sh
sh webcam.sh /dev/video1 30 foobar
```

### Segunda abordagem

O seguinte comando iniciará a aplicação e buscará pelas imagens indicadas no argumento passado. O arquivo com os pontos faciais serão gravados no diretório Embeddings com o nome foobar.pickle.
```sh
sh pic.sh "path_to_folder" foobar
```


## Treinamento da rede

Para iniciar o treinamento da rede é encessário executar o script train.py. Essa operação é realizada pela sequência de comandos fornecidas pelo script update.sh

```sh
sh update.sh
```

Nesse caso, o script de treinamento está no diretório net. Após a execução são gerados os arquivos de inicialização da rede neural um arquivo XML contendo o nome das pessoas cadastradas na base de dados.

```sh
cd ./net

rm -r ./train_lmdb/*
rm -r ./test_lmdb/*
rm ./init_net.pb
rm ./predict_net.pb
rm ./OutputLabels.xml

python ./train.py
```

# Servidor para treinamento da rede

Com objetivo de tornar a tarefa de atualização da rede neural mais simples criou-se uma aplicação que automatiza o processo de cadastro de uma pessoa. O servidor recebe o nome de uma pessoa e realiza uma série de fotos para criar o arquivo usado no processo de treinamento.
Em seguida, o treinamento da rede neural é iniciado automaticamente e o modelo gerado é gravado em um diretório compartilhado.

Assim, uma aplicação de reconhecimento pode ser atualizada em tempo de execução como demonstrado [neste projeto](https://gitlab.com/qualcomm-iot-reference-center/facial_recognition_using_caffe2).

Para tal, é necessário instalar um utilitário FTP na máquina de desenvolvimento.

```sh
sudo apt-get install vsftpd
```

Para cadastrar uma nova pessoa basta executar o seguinte comando:

```sh
python main.py
```
