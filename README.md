# CNN training for facial recognition

This paper aims to exemplify and demonstrate the process of training a convolutional network using the Caffe2 framework for facial recognition applications

# Training Environment (PC - Linux)

To perform CNN training, you must install Anaconda on the development machine.

#### - Step 1: Installing anaconda2 for python 

Download: https://www.anaconda.com/download/#linux
For other architectures, check the options in the link provided.

```sh
cd /home/${USER}
wget https://repo.continuum.io/archive/Anaconda2-2018.12-Linux-x86_64.sh
```


Command for installation: https://conda.io/docs/user-guide/install/linux.html

```sh
bash Anaconda2-2018.12-Linux-x86_64.sh
```

Confirm the installation license and directory (/home/${USER}/anaconda2). Then test the utility.

```sh
source ~/.bashrc
conda info
```


#### - Step 2: Installing Opencv on the development machine

```sh
sudo apt-get install libgtk2.0-dev
sudo apt-get install pkg-config
sudo apt-get install python-opencv
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module
```


#### - Step 3: Installing Caffe2 on the development machine

Download the lmdb library:

```sh
sudo apt-get update 
sudo apt-get install liblmdb-dev
```

Download the pytorch repository:

```sh
$ cd /home/${USER}
$ git clone --recursive https://github.com/pytorch/pytorch.git && cd pytorch
$ git submodule update --init --recursive
```


In the pytorch directory, run the caffe2 compile script:

```sh
./scripts/build_local.sh -DUSE_LMDB=ON
cd build
sudo ninja install
```


It should be noted that the PYTHONPATH environment variable must be updated containing the caffe2 directory.

```sh
export PYTHONPATH=/home/$USER/pytorch/build:$PYTHONPATH
```

#### - Step 4: Configuring Python 2.7

Need to install Python 2.7 packages:

```sh
pip install imutils
pip install matplotlib
pip install numpy==1.15.0
pip install face_recognition
pip install google
pip install lmdb
pip install IPython
```

# Application

Download repository:

```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/mlp-for-facial-recognition-using-caffe2
cd mlp-for-facial-recognition-using-caffe2
```

## Application to create a database

It can be done in two ways:
1. Directly from a device.
2. From a directory with images.

Regardless of the approaches, the result will be a file with embeddings stored in the Embeddings directory. Subsequently, this directory is used for neural network training.

### First approach

The following command launches the application to capture 30 images using the device represented by /dev/video1. The file with the facial points will be saved in the Embeddings directory with the name foobar.pickle.

```sh
sh webcam.sh /dev/video1 30 foobar
```

### Second approach

The following command launches the application and searches for the images indicated in the last argument. The file with the facial points will be saved in the Embeddings directory with the name foobar.pickle.

```sh
sh pic.sh "path_to_folder" foobar
```

## Network Training

Run the train.py script to start the training of the network. This operation is performed by the sequence of commands provided by the update.sh script.

```sh
sh update.sh
```

In this case, the training script is in the net directory. After execution, the neural network initialization files are generated an XML file containing the names of the people registered in the database.

```sh
cd ./net

rm -r ./train_lmdb/*
rm -r ./test_lmdb/*
rm ./init_net.pb
rm ./predict_net.pb
rm ./OutputLabels.xml

python ./train.py
```

# Server for network training

To make the task of updating the neural network simpler an application was created that automates the process of registering a person. The server receives the name of a person and performs a series of photos to create the file used in the training process.
Then, neural network training starts automatically, and the generated template is written to a shared directory.

Thus, a recognition application can be updated at run time as demonstrated [in this project] (https://gitlab.com/qualcomm-iot-reference-center/facial_recognition_using_caffe2).

For that, it is necessary to install an FTP utility on the development machine.


```sh
sudo apt-get install vsftpd
```


To register a new person, just execute the following command:

```sh
python main.py
```