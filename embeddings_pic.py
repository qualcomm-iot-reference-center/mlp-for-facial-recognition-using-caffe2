# USAGE
# python facial_landmarks.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg 

# import the necessary packages
from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import face_recognition
import pickle
import sys
import os



# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", required=True,
	help="path to facial landmark predictor")
ap.add_argument("-f", "--folder", required=True,
	help="path to folder")
ap.add_argument("-n", "--name", required=True,
	help="name")

args = vars(ap.parse_args())



# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
print ('\n********************************************')
print ('loading face detector')
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(args["shape_predictor"])


name = args["name"]
folder = args["folder"]


knownEncodings = []
knownNames = []


dataset_list = []


for filename in os.listdir(folder):
	dataset_list.append(folder + '/' + filename)



for img_src in dataset_list: 

	image = cv2.imread(img_src)

	if (image is not None):

		image = imutils.resize(image, width=320)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		rects = detector(gray, 1)

		for (i, rect) in enumerate(rects):
			shape = predictor(gray, rect)
			shape = face_utils.shape_to_np(shape)

			boxes = face_recognition.face_locations(image,model="cnn")
			encoding = face_recognition.face_encodings(image, boxes)

			# convert dlib's rectangle to a OpenCV-style bounding box
			# [i.e., (x, y, w, h)], then draw the face bounding box
			(x, y, w, h) = face_utils.rect_to_bb(rect)
			boxes = [(y, x+w, y+h, x)]				
			print(encoding)
			knownEncodings.append(encoding[0])
			knownNames.append(name)



print("[INFO] serializing encodings...")
data = {"embeddings": knownEncodings, "labels": knownNames}
f = open('./Embeddings/'+name+".pickle", "wb")
f.write(pickle.dumps(data))
f.close()


cv2.destroyAllWindows()
