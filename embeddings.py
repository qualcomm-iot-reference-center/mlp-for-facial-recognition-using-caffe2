# USAGE
# python facial_landmarks.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg 

# import the necessary packages
import imutils
from imutils import face_utils
import dlib
import face_recognition
import pickle

import numpy as np
import cv2

import argparse
import sys,os

import time
from multiprocessing import Pool, Process, Manager, Queue, Event

def VideoCapture(vobj, interval, q, event, total):

	#starts video capture
	cv2.namedWindow("Camera")

	start = time.time()
	init = 0

	counter = 0

	while True:
		#reads a new frame 
		status, image = vobj.read()

		if (image is not None):
			#updates timer interval
			end = time.time()

			ym = int(image.shape[0]/2)
			ys = int(image.shape[0]*0.25)

			xm = int(image.shape[1]/2)
			xs = int(image.shape[1]*0.2)

			cv2.rectangle(image, (xm-xs,ym-ys), (xm+xs, ym+ys), (0, 0, 255), 2)

			#evaluates the time interval
			if ((end - start) >= interval):

				#Takes a snapshot only if the internal flag is true
				#or if this is the first iteration

				if (init == 0) or (event.is_set()):
				
					#resizes the image and convert it to gray color
					#img = imutils.resize(image, width=320)
					img = image[ym-ys+2:ym+ys-2, xm-xs+2:xm+xs-2]
					gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

					#try to detect faces int the image
					rects = detector(gray, 1)
					
					if (len(rects) > 0):
						#restarts timer interval
						start = time.time()

						#passes the image for the consumer process
						q.put([img, gray, rects])

						counter = counter + 1
					
						#Resets the internal flag to false
						if(init == 1):
							event.clear()

					#sets the first iteration flag
					init = 1
				
			#shows the current image caught by camera
			cv2.putText(image, "Counter: " + str(counter), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
			cv2.imshow("Camera", image)

		#gets keypressed'event
		if ((cv2.waitKey(1) & 0xFF) == ord('q')) or (counter >= total):
			#signals consumer to exit process
			q.put([None, None, None])
			time.sleep(3)
			break
 

def FacialRecognizer(image,gray,rect,procnum,my_return):
	shape = predictor(gray, rect)
	shape = face_utils.shape_to_np(shape)

	# convert dlib's rectangle to a OpenCV-style
	(x, y, w, h) = face_utils.rect_to_bb(rect)
	boxes = [(y, x+w, y+h, x)]				
	
	encoding = face_recognition.face_encodings(image, boxes)

	my_return[procnum] = [boxes[0],encoding[0]]

def CreateFile(knownEncodings, name):
	knownNames = []

	for i in range(len(knownEncodings)):
		knownNames.append(name)

	print("[INFO] serializing encodings...")

	data = {"embeddings": knownEncodings, "labels": knownNames}
	f = open('./Embeddings/'+name+".pickle", "wb")
	f.write(pickle.dumps(data))
	f.close()

def FeatureExtraction(q,event):
	cv2.namedWindow("Snapshot")

	knownEncodings = []
	
	while True:
		#waits for a new image from producer process
		image, gray, rects = q.get()

		if (image is None):
			CreateFile(knownEncodings,name)
			#breaks the process
			break

		
		parallel_process = []
		manager = Manager()
		return_dict = manager.dict()

		#starts a new process for each face detected
		for (i, rect) in enumerate(rects):
			parallel_process.append(Process(target=FacialRecognizer, args=(image,gray,rect,i,return_dict)))
			parallel_process[i].start()

		#waits for processes completion
		for (i, p) in enumerate(parallel_process):
			p.join()

			#gets the result of current process
			boxes = return_dict[i][0]
			knownEncodings.append(return_dict[i][1])

			cv2.rectangle(image, (boxes[3],boxes[0]), (boxes[1], boxes[2]), (0, 0, 255), 2)

		#notifies the producer process that this task have been completed
		event.set()

		# shows the output image
		cv2.imshow("Snapshot", image)
		cv2.waitKey(1)
	 


if __name__ == '__main__':

	# construct the argument parser and parse the arguments
	ap = argparse.ArgumentParser()
	ap.add_argument("-p", "--shape-predictor", required=True,
		help="path to facial landmark predictor")
	ap.add_argument("-i", "--image", required=True,
		help="path to input image")
	ap.add_argument("-t", "--frameInterval", required=True,
		help="image Processing Interval in milliseconds")
	ap.add_argument("-n", "--name", required=True,
		help="name")
	ap.add_argument("-f", "--frames", required=True,
		help="frame")

	args = vars(ap.parse_args())

	# Default imageProcessing interval in seconds
	imageProcessingInterval = int(args["frameInterval"])


	# initialize dlib's face detector (HOG-based) and then create
	# the facial landmark predictor
	print ('\n********************************************')
	print ('loading face detector')
	print ('********************************************')
	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor(args["shape_predictor"])

	print ('\n********************************************')
	print ('starting video capture')
	print ('********************************************')
	#vobj = cv2.VideoCapture(int(args["image"][-1]))
	#print(args["image"])
	#vobj = cv2.VideoCapture(args["image"])
	#vobj = cv2.VideoCapture(1)
	vobj = cv2.VideoCapture(int(args["image"]))


	name = args["name"]
	pics = int(args["frames"])


	#shared signal between prod/consumer 
	evt = Event()

	#shared data between prod/consumer 
	q = Queue()

	process_one = Process(target=VideoCapture, args=(vobj, imageProcessingInterval, q, evt,pics))
	process_two = Process(target=FeatureExtraction, args=(q,evt))

	#starts prod/consumer process
	process_one.start()
	process_two.start()

	#release shared data
	q.close()
	q.join_thread()

	#waits for the end of process
	process_one.join()
	process_two.join()

	vobj.release()
	cv2.destroyAllWindows()

	sys.exit(0)
