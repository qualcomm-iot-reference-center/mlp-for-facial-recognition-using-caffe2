import shlex, subprocess

def python_to_bash(cmd_bash):
    args = shlex.split(cmd_bash)
    child = subprocess.Popen(args, stdout=subprocess.PIPE)
    outputs, errors = child.communicate()
    return child.returncode

print("Digite o nome:")
name = raw_input()
print(name)
status = python_to_bash("python embeddings.py -p shape_predictor_68_face_landmarks.dat -i 2 -t 1 -f 30 -n " + name)

if status == 0:
    print("Embeddings OK")
    
    status = python_to_bash("sh update.sh")

    if status == 0:
        print("Training OK")
        python_to_bash("sh zip.sh")
	python_to_bash("cp ./net/net.zip /srv/tftpboot")
    else:
        print("Training ERROR")
else:
    print("Embeddings ERROR")
